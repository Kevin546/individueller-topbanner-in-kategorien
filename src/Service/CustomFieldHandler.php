<?php declare(strict_types=1);

namespace CategoryTopBanner\Service;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\CustomField\CustomFieldTypes;

class CustomFieldHandler {
    /**
     * @var EntityRepositoryInterface
     */
    private $customFieldSetRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $customFieldRepository;

        /**
     * @var EntityRepositoryInterface
     */
    private $snippetRepository;

    public function __construct(
        EntityRepositoryInterface $customFieldSetRepository,
        EntityRepositoryInterface $customFieldRepository,
        EntityRepositoryInterface $snippetRepository
    ) {
        $this->customFieldSetRepository = $customFieldSetRepository;
        $this->customFieldRepository = $customFieldRepository;
        $this->snippetRepository = $snippetRepository;
    }

    private function getCustomFieldConfig()
    {
        return [
            [
                'name' => 'custom_category_top_banner',
                'config' => [
                    'label' => [
                        'en-GB' => 'Top banner',
                        'de-DE' => 'Topbanner'
                    ]
                ],
                'relations' => [
                    [
                        'entityName' => 'category',
                    ]
                ],
                'customFields' => [
                    [
                        'name' => 'custom_category_top_banner_media',
                        'type' => 'text',
                        'config' => [
                            'customFieldPosition' => 1,
                            'componentName' => 'sw-media-field',
                            'customFieldType' => 'media',
                            'label' => [
                                'en-GB' => 'Top banner media',
                                'de-DE' => 'Topbanner Medien'
                            ]
                        ]
                    ],
                    [
                        'name' => 'custom_category_top_banner_title',
                        'type' => 'text',
                        'config' => [
                        'customFieldPosition' => 2,
                        'componentName' => 'sw-text-field',
                        'customFieldType' => 'text',
                            'label' => [
                                'en-GB' => 'Top banner title',
                                'de-DE' => 'Topbanner Titel'
                            ]
                        ]
                    ],
                    [
                        'name' => 'custom_category_top_banner_text',
                        'type' => 'text',
                        'config' => [
                        'customFieldPosition' => 3,
                        'componentName' => 'sw-text-field',
                        'customFieldType' => 'text',
                            'label' => [
                                'en-GB' => 'Top banner text',
                                'de-DE' => 'Topbanner Text'
                            ]
                        ]
                    ],
                    [
                        'name' => 'custom_category_top_banner_height',
                        'type' => 'text',
                        'config' => [
                        'customFieldPosition' => 4,
                        'componentName' => 'sw-text-field',
                        'customFieldType' => 'text',
                            'label' => [
                                'en-GB' => 'Top banner height',
                                'de-DE' => 'Topbanner Höhe'
                            ]
                        ]
                    ]       
                ] 
            ]
        ];
    }


    public function addIfNotExists()
    {
        $this->removeOld();

        foreach ($this->getCustomFieldConfig() as $config) {
            $customFieldSet = $this->getCustomFieldSet($config['name']);

            if (!$customFieldSet) {
                $this->createCustomFieldset($config);
            }

            foreach ($config['customFields'] as $field) {
                if (!$this->getCustomField($config['name'], $field['name'])) {
                    $this->createCustomField($config['name'], $field);
                }
            }
        }
    }

    public function removeIfExists()
    {
        $this->removeOld();

        foreach ($this->getCustomFieldConfig() as $config) {
            foreach ($config['customFields'] as $field) {
                $customField = $this->getCustomField($config['name'], $field['name']);

                if ($customField) {
                    $this->removeCustomField($customField->getId());
                }
            }

            $customFieldSet = $this->getCustomFieldSet($config['name']);

            if ($customFieldSet) {
                $this->removeCustomFieldset($customFieldSet->getId());
            }
        }
    }

    private function getCustomFieldSet($name)
    {
        if (!$name) {
            return;
        }

        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $name));

        $result = $this->customFieldSetRepository->search($criteria, Context::createDefaultContext());

        return $result->first();
    }

    private function getCustomSnippet($name)
    {
        if (!$name) {
            return;
        }

        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('translationKey', $name));

        $result = $this->snippetRepository->search($criteria, Context::createDefaultContext());

        return $result->first();
    }

    private function createCustomFieldset($customFieldSet)
    {
        if (
            !$customFieldSet['name'] ||
            !$customFieldSet['config'] ||
            !$customFieldSet['relations']
        ) {
            return;
        }

        $this->customFieldSetRepository->create([
            [
                'id' => Uuid::randomHex(),
                'name' => $customFieldSet['name'],
                'config' => $customFieldSet['config'],
                'relations' => $customFieldSet['relations']
            ]
        ], Context::createDefaultContext());
    }

    private function getCustomField($setName, $fieldName)
    {
        if (!$setName || !$fieldName) {
            return;
        }

        $customFieldSet = $this->getCustomFieldSet($setName);

        if (!$customFieldSet) {
            return;
        }

        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $fieldName))
            ->addFilter(new EqualsFilter('customFieldSetId', $customFieldSet->getId()));

        return $this->customFieldRepository
            ->search($criteria, Context::createDefaultContext())
            ->first();
    }

    private function createCustomField($setName, $customField)
    {
        if (
            !$customField['name'] ||
            !$customField['config']
        ) {
            return;
        }

        $customFieldSet = $this->getCustomFieldSet($setName);

        if (!$customFieldSet) {
            return;
        }

        $this->customFieldRepository->create([
            [
                'id' => Uuid::randomHex(),
                'name' => $customField['name'],
                'type' => $customField['type'],
                'config' => $customField['config'],
                'customFieldSetId' => $customFieldSet->getId()
            ]
        ], Context::createDefaultContext());
    }

    private function removeCustomField($id)
    {
        $this->customFieldRepository->delete([['id' => $id]], Context::createDefaultContext());
    }

    private function removeCustomFieldset($id)
    {
        $this->customFieldSetRepository->delete([['id' => $id]], Context::createDefaultContext());
    }

    private function removeSnippets($id)
    {
        $this->snippetRepository->delete([['id' => $id]], Context::createDefaultContext());
    }

    private function removeOld()
    {
        $customConfig = [
            'custom_category_top_banner' => [
                'custom_category_top_banner_media',
                'custom_category_top_banner_title',
                'custom_category_top_banner_text',
                'custom_category_top_banner_height'
            ]
        ];

        $customSnippets = [       
            'customFields.custom_category_top_banner_media',
            'customFields.custom_category_top_banner_title',    
            'customFields.custom_category_top_banner_text',    
            'customFields.custom_category_top_banner_height',        
        ];

        foreach ($customConfig as $customSetName => $customFields) {
            foreach ($customFields as $field) {
                $customField = $this->getCustomField($customSetName, $field);

                if ($customField) {
                    $this->removeCustomField($customField->getId());
                }
            }
            

            $customFieldSet = $this->getCustomFieldSet($customSetName);

            if ($customFieldSet) {
                $this->removeCustomFieldset($customFieldSet->getId());
            }
        }

        foreach ($customSnippets as $field) {
            $customSnippet = $this->getCustomSnippet($field);

            if ($customSnippet) {
                $this->removeSnippets($customSnippet->getId());
            }
        }
    }
}
